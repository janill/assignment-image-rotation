#include "file_management.h"

#include <errno.h>

static const char* const MODES[] = {
    [FOM_READ_BINARY] = "rb",
    [FOM_WRITE_BINARY] = "wb"
};

enum file_open_status open_file( const char* filepath, enum file_open_mode mode, FILE** file ) {
    FILE* opened_file;

    opened_file = fopen(filepath, MODES[mode]);

    if ( opened_file != NULL ) {
        *file = opened_file;
        return FOS_OK;
    }

    int err_num = errno;

    switch (err_num) {
    case ENOENT:
        return FOS_NOT_FOUND;
    case EACCES:
        return FOS_NO_RIGHTS;    
    default:
        return FOS_UNKNOWN;
    }
}

enum file_close_status close_file( FILE* file ) {

    if ( fclose(file) == 0) {
        return FCS_OK;
    }

    int err_num = errno;

    switch (err_num) {
    case ENOSPC:
        return FCS_NO_SPACE_LEFT;    
    default:
        return FCS_UNKNOWN;
    }
}
