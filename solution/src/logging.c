#include "logging.h"

#include <stdio.h>

static void log_prefix(const char* prefix, const char* msg) {
    fprintf(stderr, "%s : %s\n", prefix, msg);
}

void log_info(const char* msg) {
    log_prefix("INFO", msg);
}

void log_warn(const char* msg) {
    log_prefix("WARNING", msg);
}

void log_error(const char* msg) {
    log_prefix("ERROR", msg);
}
