#include <inttypes.h>
#include <stdio.h>

#include "ascii_art.h"
#include "bmp_file_io.h"
#include "image.h"
#include "image_transform.h"
#include "logging.h"

static const int ERROR_CODE_WRONG_USAGE = -2;
static const int ERROR_CODE_FAILED_IO = -1;

static void print_usage(void) {
    log_error("Usage: ./image-transformer <source-image> <transformed-image>");
}


int main( int argc, const char** argv ) {

    if ( argc < 3 ) {
        log_error("Not enough parameters!");
        print_usage();
        return ERROR_CODE_WRONG_USAGE;
    }

    if ( argc > 3) {
        log_error("Too much parameters");
        print_usage();
        return ERROR_CODE_WRONG_USAGE;
    }

    const char* in_filepath = argv[1];

    const char* out_filepath = argv[2];

    struct image image = {0};

    if ( !read_from_bmp(in_filepath, &image) ) {
        return ERROR_CODE_FAILED_IO;
    }

    struct image rotated = rotate(image);

    if ( !write_to_bmp(out_filepath, rotated) ) {
        return ERROR_CODE_FAILED_IO;
    }

    img_destroy(image);
    img_destroy(rotated);

    return 0;
}
