#include "bmp_io.h"

#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>


static const size_t PIXEL_SIZE_BYTES = sizeof(struct pixel);
static const size_t SUPPORTED_BIT_DEPTH = 24;

static const uint32_t INFO_HEADER_SIZE_BYTES = 40;

static const uint16_t HEADER_N_PLANES = 1;
static const uint32_t HEADER_NO_COMPRESSION = 0;
static const uint32_t HEADER_PPM = 2834;
static const uint32_t HEADER_COLORS = 0;


static const int SYSTEM_ERROR_CODE = -1;

struct __attribute__((packed)) bmp_header {
            uint8_t bfType1;
            uint8_t bfType2;
            uint32_t  bfileSize;
            uint32_t bfReserved;
            uint32_t bOffBits;
            uint32_t biSize;
            uint32_t biWidth;
            uint32_t  biHeight;
            uint16_t  biPlanes;
            uint16_t biBitCount;
            uint32_t biCompression;
            uint32_t biSizeImage;
            uint32_t biXPelsPerMeter;
            uint32_t biYPelsPerMeter;
            uint32_t biClrUsed;
            uint32_t  biClrImportant;
    };

static bool fread_bmp_header(FILE* file, struct bmp_header* bmp_header) {
    if (!file || !bmp_header) {
        return false;
    }

    if ( fseek(file, 0, SEEK_SET) == SYSTEM_ERROR_CODE ) {
        return false;
    }
    

    return fread(bmp_header, sizeof(struct bmp_header), 1, file);
}


inline static size_t img_padding_bytes( uint32_t img_width_pixels ) {
    return img_width_pixels % 4;
}

inline static bool valid_signature( struct bmp_header const* header ) {
    return header->bfType1 == 'B' && header->bfType2 == 'M';
}

static bool read_bitmap( FILE* in, struct image* img, size_t bitmap_offset ) {

    size_t padding_bytes = img_padding_bytes(img->width);


    // move cursor to the start of the bitmap
    if( fseek(in, (long) bitmap_offset, SEEK_SET) == SYSTEM_ERROR_CODE) {
        return false;
    }

    for (size_t i = 0; i < img->height; i++) {
        
        size_t n_items_read = fread(img_get_pixel(*img, 0, i), sizeof(struct pixel), img->width, in);

        if ( n_items_read != img->width ) {
            return false;
        }

        //skip the padding bytes
        if( fseek(in, (long) padding_bytes, SEEK_CUR) == SYSTEM_ERROR_CODE ) {
            return false;
        }

    }

    return true;
}



enum read_status from_bmp( FILE* in, struct image* img ) {

    struct bmp_header header;


    if ( !fread_bmp_header(in, &header) ) {
        return READ_INVALID_HEADER;
    }

    if ( !valid_signature(&header) ) {
        return READ_INVALID_SIGNATURE;
    }

    if ( header.biBitCount != SUPPORTED_BIT_DEPTH ) {
        return READ_INVALID_BIT_DEPTH;
    }
    

    // biHeight, biWidth are in px
    struct image tmp_img = img_create(header.biHeight, header.biWidth);


    if ( !read_bitmap(in, &tmp_img, header.bOffBits) ) {
        img_destroy(tmp_img);
        return READ_INVALID_BITS;
    }

    *img = tmp_img;

    return READ_OK;
}

static uint32_t img_size_in_bytes( const struct image  img ) {
    uint32_t padding_bytes = img_padding_bytes(img.width);

    return img.width * img.height * PIXEL_SIZE_BYTES + (img.height + 1) * padding_bytes;
}

static struct bmp_header compose_header( const struct image  img ) {
    struct bmp_header header = {0};

    const uint32_t RESERVED = 0; 

    header.bfType1 = 'B';
    header.bfType2 = 'M';
    header.bfileSize = img_size_in_bytes(img) + sizeof(struct bmp_header);
    header.bfReserved = RESERVED;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = INFO_HEADER_SIZE_BYTES;
    header.biWidth = img.width;
    header.biHeight = img.height;
    header.biPlanes = HEADER_N_PLANES;
    header.biBitCount = SUPPORTED_BIT_DEPTH;
    header.biCompression = HEADER_NO_COMPRESSION;
    header.biSizeImage = img_size_in_bytes(img);
    header.biYPelsPerMeter = HEADER_PPM;
    header.biXPelsPerMeter = HEADER_PPM;
    header.biClrUsed = HEADER_COLORS;
    header.biClrImportant = HEADER_COLORS;

    return header;
}

static bool write_header( FILE* out, struct bmp_header const* header ) {

    if( fseek(out, 0, SEEK_SET) == SYSTEM_ERROR_CODE) {
        return false;
    }

    return fwrite(header, sizeof(struct bmp_header), 1, out);
}


static bool write_bitmap( FILE* out, const struct image image, size_t bitmap_offset ) {
    size_t n_padding_bytes = img_padding_bytes(image.width);
    const uint8_t garbage[3] = {0, 0, 0};

    if( fseek(out, (long) bitmap_offset, SEEK_SET) == SYSTEM_ERROR_CODE ) {
        return false;
    } 

    for (size_t i = 0; i < image.height; i++) {

        size_t n_items_written = fwrite(img_get_pixel(image, 0, i), sizeof(struct pixel), image.width, out);

        if ( n_items_written != image.width ) {
            return false;
        }

        size_t n_padding_written = fwrite(garbage, sizeof(uint8_t), n_padding_bytes, out);

        if ( n_padding_written != n_padding_bytes ) {
            return false;
        }
    }

    return true;
}

enum write_status to_bmp( FILE* out, const struct image img ) {
    struct bmp_header header = compose_header(img);

    if ( !write_header(out, &header) ) {
        return WRITE_HEADER_FAILED;
    }

    if ( !write_bitmap(out, img, header.bOffBits) ) {
        return WRITE_BITS_FAILED;
    }

    return WRITE_OK;

}

