#include "ascii_art.h"

#include <inttypes.h>
#include <stdio.h>

#include "pixel.h"

static const float R_CONSTANT = 0.299f;
static const float G_CONSTANT = 0.587f;
static const float B_CONSTANT = 0.114f;

static float pixel_get_grayscale_value( struct pixel pixel ) {
    return  R_CONSTANT * ((float) pixel.r / 255.0f) + 
            G_CONSTANT * ((float) pixel.g / 255.0f) + 
            B_CONSTANT * ((float) pixel.b / 255.0f);
}

static char pixel_get_ascii_char( struct pixel pixel ) {
    // between 0 and 1
    float gs_val = pixel_get_grayscale_value(pixel);

    if ( 0.0 <= gs_val && gs_val < 0.1 )
        return '.';
    if ( 0.1 <= gs_val && gs_val < 0.2 )
        return ',';
    if ( 0.2 <= gs_val && gs_val < 0.3 )
        return ';';  
    if ( 0.3 <= gs_val && gs_val < 0.4 )
        return '!';
    if ( 0.4 <= gs_val && gs_val < 0.5 )
        return 'v';
    if ( 0.5 <= gs_val && gs_val < 0.6 )
        return 'l';    
    if ( 0.6 <= gs_val && gs_val < 0.7 )
        return 'L';
    if ( 0.7 <= gs_val && gs_val < 0.8 )
        return 'F';
    if ( 0.8 <= gs_val && gs_val < 0.9 )
        return 'E';  

    return '$';
}



void img_print_ascii( struct image const* img, FILE* stream ) {

    for (size_t i = 0, chars_in_line = 0; i < img->height * img->width; i++, chars_in_line++) {

        struct pixel pixel = img->data[i];
        char c = pixel_get_ascii_char(pixel);
        fputc(c, stream);

        if ( chars_in_line != 0 && chars_in_line % img->width == 0 ) {
            fputc('\n', stream);
        }
    }
}
