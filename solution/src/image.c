#include "image.h"

#include <malloc.h>

struct image img_create(uint32_t height, uint32_t width) {
    struct image img = {0};

    img.height = height;
    img.width = width;

    img.data = malloc(sizeof(struct pixel) * height * width);

    return img;
}

void img_destroy(struct image img) {
    free(img.data);
}


struct pixel* img_get_pixel( const struct image src, const uint32_t x, const uint32_t y) {
    if ( x >= src.width || y >= src.height ) {
        return NULL;
    }

    return src.data + x + y * src.width;
}

