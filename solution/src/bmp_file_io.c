#include "bmp_file_io.h"

#include <malloc.h>
#include <stdbool.h>
#include <stdio.h>

#include "file_management.h"
#include "logging.h"
#include "string_lib.h"

static const char* const FOS_MSGS[] = {
    [FOS_OK] = "File was successfully opened ",
    [FOS_NOT_FOUND] = "File not found ",
    [FOS_NO_RIGHTS] = "No permission to open file ",
    [FOS_UNKNOWN] = "Something went wrong with opening " 
};

static const char* const FCS_MSGS[] = {
    [FCS_OK] = "File was successfully closed ",
    [FCS_NO_SPACE_LEFT] = "File can not be closed: no space left on the device ",
    [FCS_UNKNOWN] = "Something went wrong with closing "
};

static const char* const READ_ERROR_MSGS[] = {
    [READ_INVALID_SIGNATURE] = "Error! Looks like that file isn't BMP",
    [READ_INVALID_BITS] = "Error! Looks like file is corrupted",
    [READ_INVALID_HEADER] = "Error! Looks like BMP header is corrupted",
    [READ_INVALID_BIT_DEPTH] = "Error! Only 24-bit BMP files are supported"
};

static const char* const WRITE_ERROR_MSG = "Error! Can't write to the file";

static void log_fopen( enum file_open_status status, const char* filepath ) {

    const char* msg = FOS_MSGS[status];

    char* f_msg = c_str_concat(msg, filepath);

    if (status == FOS_OK) {
        log_info(f_msg);
    }
    else {
        log_error(f_msg);
    }
    
    free(f_msg);
}

static void log_fclose( enum file_close_status status, const char* filepath ) {
    const char* msg = FCS_MSGS[status];

    char* f_msg = c_str_concat(msg, filepath);

    if (status == FCS_OK) {
        log_info(f_msg);
    }
    else {
        log_error(f_msg);
    }

    free(f_msg);
}

bool read_from_bmp( const char* filepath, struct image* image ) {
    FILE * file;

    enum file_open_status fos = open_file(filepath, FOM_READ_BINARY, &file);

    log_fopen(fos, filepath);

    if (fos != FOS_OK) {
        return false;
    }

    enum read_status r_status = from_bmp(file, image);

    if ( r_status != READ_OK ) {
        log_error(READ_ERROR_MSGS[r_status]);
    }

    enum file_close_status fcs = close_file(file);

    log_fclose(fcs, filepath);

    return r_status == READ_OK && fcs == FCS_OK;
}

bool write_to_bmp(const char* filepath, const struct image src) {
    FILE* file;

    enum file_open_status fos = open_file(filepath, FOM_WRITE_BINARY, &file);

    log_fopen(fos, filepath);

    if (fos != FOS_OK) {
        return false;
    }

    enum write_status w_status = to_bmp(file, src);

    if ( w_status != WRITE_OK ) {
        log_error(WRITE_ERROR_MSG);
    }

    enum file_close_status fcs = close_file(file);

    log_fclose(fcs, filepath);

    return w_status == WRITE_OK && fcs == FCS_OK;
}
