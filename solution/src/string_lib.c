#include "string_lib.h"

#include <malloc.h>
#include <string.h>

char* c_str_concat(const char* c_str1, const char* c_str2) {

    size_t len1 = strlen(c_str1);
    size_t len2 = strlen(c_str2);

    size_t out_size = len1 + len2 + 1;

    char* out = malloc(out_size * sizeof(char));

    for (size_t i = 0; i < len1; i++) {
        out[i] = c_str1[i];
    }

    for (size_t i = 0; i< len2; i++) {
        out[i + len1] = c_str2[i];
    }

    out[out_size - 1] = '\0';

    return out;
}
