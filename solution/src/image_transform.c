#include "image_transform.h"

#include <inttypes.h>
#include <memory.h>
#include <stdint.h>
#include <stdio.h>


struct image rotate(const struct image  source) {
    const uint32_t new_height =  source.width, 
                   new_width  =  source.height;

    struct image rotated = img_create(new_height, new_width);

    for (size_t i = 0; i < source.height; i++) {
        
        for (size_t j = 0; j < source.width; j++) {

            const struct pixel* src = img_get_pixel(source, j, source.height - i - 1);

            struct pixel* dest = img_get_pixel(rotated, i, j);

            *dest = *src;
        }
    }

    return rotated;
}
