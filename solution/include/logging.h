#ifndef LOGGING_H
    #define LOGGING_H

    void log_info(const char* msg);

    void log_warn(const char* msg);

    void log_error(const char* msg);

#endif
