#ifndef BMP_IO_H
    #define BMP_IO_H

    #include <stdio.h>

    #include "image.h"

    /*  deserializer   */
    enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_BIT_DEPTH

    };

    enum read_status from_bmp( FILE* in, struct image* img );

    /*  serializer   */
    enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_HEADER_FAILED,
    WRITE_BITS_FAILED
    };

    enum write_status to_bmp( FILE* out, const struct image img );

#endif
