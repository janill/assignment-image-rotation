#ifndef FILE_MANAGEMENT_H
    #define FILE_MANAGEMENT_H

    #include <stdio.h>

    enum file_open_status {
        FOS_OK = 0,
        FOS_NOT_FOUND,
        FOS_NO_RIGHTS,
        FOS_UNKNOWN
    };

    enum file_close_status {
        FCS_OK = 0,
        FCS_NO_SPACE_LEFT,
        FCS_UNKNOWN
    };

    enum file_open_mode {
        FOM_READ_BINARY = 0,
        FOM_WRITE_BINARY
        // and so on
    };

    enum file_open_status open_file( const char* filepath, enum file_open_mode mode, FILE** file );

    enum file_close_status close_file( FILE* file );

#endif
