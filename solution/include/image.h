#ifndef IMAGE_H
    #define IMAGE_H

    #include <stdint.h>

    #include "pixel.h"

    struct image {
        uint32_t height, width;

        struct pixel* data;
    };

    // return address of the pixel at the given location, NULL if index out of bounds for the current image
    struct pixel* img_get_pixel( const struct image src, const uint32_t x, const uint32_t y );


    struct image img_create(uint32_t height, uint32_t width);

    void img_destroy(struct image img);
    
#endif
