#ifndef IMAGE_TRANSFORM_H
    #define IMAGE_TRANSFORM_H

    #include "image.h"

    struct image rotate( const struct image source );

#endif
