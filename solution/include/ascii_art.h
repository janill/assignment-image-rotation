#ifndef ASCII_ART_H
    #define ASCII_ART_H

    #include "image.h"

    #include <stdio.h>

    void img_print_ascii(struct image const* img, FILE* stream);

#endif
