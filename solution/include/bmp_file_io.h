#ifndef BMP_FILE_IO 
    #define BMP_FILE_IO

    #include "bmp_io.h"

    #include <stdbool.h>

    bool read_from_bmp(const char* filepath, struct image* image);

    bool write_to_bmp(const char* filepath, const struct image src);

#endif
